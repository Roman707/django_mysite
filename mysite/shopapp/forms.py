from django.contrib.auth.models import Group
from django.forms import ModelForm
from shopapp.models import Product, Order
from django import forms


class GroupForm(ModelForm):
    class Meta:
        model = Group
        fields = ['name']


class ProductForm(ModelForm):
    class Meta:
        model = Product
        fields = 'name', 'price', 'description', 'discount', 'preview',

    images = forms.ImageField(
        widget=forms.ClearableFileInput(attrs={'multiple': True}))


class OrderForm(ModelForm):
    class Meta:
        model = Order
        fields = 'user', 'products', 'delivery_address', 'promocode'


class CSVImportForm(forms.Form):
    csv_file = forms.FileField()
