from django.conf import settings
from django.contrib.auth.models import User, Permission, PermissionsMixin
from django.test import TestCase
from django.urls import reverse

from string import ascii_letters
from random import choices

from shopapp.models import Product, Order, User
from shopapp.utils import add_two_numbers


# python manage.py dumpdata shopapp > shopapp-fixtures.json  команда для выгрузки информации из приложения shopapp
# python manage.py loaddata shopapp-fixtures.json  команда для восстановления информации в shopapp

class AddTwoNumbersTestCase(TestCase):
    def test_add_two_numbers(self):
        result = add_two_numbers(2, 3)
        self.assertEqual(result, 5)


class ProductCreateViewTestCase(TestCase):
    def setUp(self) -> None:
        self.product_name = ''.join(choices(ascii_letters, k=10))
        Product.objects.filter(name=self.product_name).delete()

    def test_create_product(self):
        response = self.client.post(
            reverse('shopapp:product_create'),
            {
                'name': self.product_name,
                'price': '123.45',
                'description': 'A good table',
                'discount': '10',
            },
            HTTP_USER_AGENT='Mozilla/5.0',
        )
        self.assertRedirects(response, reverse('shopapp:products_list'))
        self.assertTrue(Product.objects.filter(name=self.product_name).exists())


class ProductDetailsViewTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        cls.product = Product.objects.create(name='Best Product')

    @classmethod
    def tearDownClass(cls):
        cls.product.delete()

    def test_get_product(self):
        response = self.client.get(
            reverse(
                'shopapp:products_details', kwargs={'pk': self.product.pk}
            ),
            HTTP_USER_AGENT='Mozilla/5.0'
        )
        self.assertEqual(response.status_code, 200)

    def test_get_product_and_check_content(self):
        response = self.client.get(
            reverse(
                'shopapp:products_details', kwargs={'pk': self.product.pk}
            ),
            HTTP_USER_AGENT='Mozilla/5.0'
        )
        self.assertContains(response, self.product.name)


class ProductListViewTestCase(TestCase):
    fixtures = [
        'products-fixture.json',
    ]

    @classmethod
    def setUpClass(cls):
        cls.products = Product.objects.filter(archived=False).all()

    @classmethod
    def tearDownClass(cls):
        cls.products.delete()

    def test_products(self):
        response = self.client.get(reverse('shopapp:products_list'), HTTP_USER_AGENT='Mozilla/5.0')
        # for product in self.products:
        #     self.assertContains(response, product.name)
        # products_ = response.context['products']
        # for p, p_ in zip(self.products, products_):
        #     self.assertEqual(p.pk, p_.pk)
        self.assertQuerysetEqual(
            qs=self.products,
            values=(p.pk for p in response.context['products']),
            transform=lambda p: p.pk,
        )
        self.assertTemplateUsed(response, 'shopapp/products-list.html')


class OrdersListViewTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        cls.user = User.objects.create_user(username='bob_test', password='qwerty')

    @classmethod
    def tearDownClass(cls):
        cls.user.delete()

    def setUp(self) -> None:
        self.client.force_login(self.user)

    def test_orders_view(self):
        response = self.client.get(
            reverse('shopapp:orders_list'), HTTP_USER_AGENT='Mozilla/5.0')
        self.assertContains(response, 'Orders')

    def test_orders_view_not_authenticated(self):
        self.client.logout()
        response = self.client.get(
            reverse('shopapp:orders_list'), HTTP_USER_AGENT='Mozilla/5.0')
        self.assertEqual(response.status_code, 302)
        self.assertIn(str(settings.LOGIN_URL), response.url)


class ProductsExportViewTestCase(TestCase):
    fixtures = [
        'products-fixture.json',
    ]

    @classmethod
    def setUpClass(cls):
        cls.products = Product.objects.order_by('pk').all()

    @classmethod
    def tearDownClass(cls):
        cls.products.delete()

    def test_get_products_view(self):
        response = self.client.get(
            reverse('shopapp:products-export'), HTTP_USER_AGENT='Mozilla/5.0',
        )
        self.assertEqual(response.status_code, 200)
        # products = Product.objects.order_by('pk').all()
        expected_data = [
            {
                'pk': product.pk,
                'name': product.name,
                'price': str(product.price),
                'archived': product.archived,
            }
            for product in self.products
        ]
        products_data = response.json()
        self.assertEqual(products_data['products'], expected_data, )


class OrderDetailViewTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        cls.user = User.objects.create_user(
            username='user_test', password='qwerty', )
        cls.user.user_permissions.add(
            Permission.objects.get(codename='view_order'))

    @classmethod
    def tearDownClass(cls):
        cls.user.delete()

    def setUp(self) -> None:
        self.client.force_login(self.user)
        self.order = Order.objects.create(
            user=self.user,
            delivery_address='ul Pupkina',
            promocode='SALE50')

    def tearDown(self) -> None:
        self.order.delete()

    def test_order_details(self):
        response = self.client.get(
            reverse('shopapp:order_details', kwargs={'pk': self.order.pk}),
            # {'user': self.user,
            #  'delivery_address': self.order.delivery_address,
            #  'promocode': self.order.promocode},
            HTTP_USER_AGENT='Mozilla/5.0')
        self.assertContains(response, self.order.delivery_address)
        self.assertContains(response, self.order.promocode)
        self.assertContains(response, self.order.pk)  # 1) как верно? или неверны оба варианта?
        self.assertEqual(response.context['order_det'].pk, self.order.pk)  # 2)


class OrdersExportTestCase(TestCase):
    fixtures = [
        'orders-fixture.json', 'products-fixture.json', 'users-fixture.json',
    ]

    @classmethod
    def setUpClass(cls):
        cls.user = User.objects.create_user(
            username='user_test', password='qwerty', is_staff=True)

    @classmethod
    def tearDownClass(cls):
        cls.user.delete()

    def setUp(self) -> None:
        self.client.force_login(self.user)

    def test_get_orders(self):
        response = self.client.get(
            reverse('shopapp:orders-export'), HTTP_USER_AGENT='Mozilla/5.0',
        )
        orders = Order.objects.order_by('pk').all()
        expected_data = [
            {
                'pk': order.pk,
                'delivery_address': order.delivery_address,
                'promocode': order.promocode,
                'user': order.user,
                'products': order.products,
            }
            for order in orders
        ]
        orders_data = response.json()
        self.assertEqual(response.status_code, 200)
        self.assertEqual(orders_data['orders'], expected_data)
