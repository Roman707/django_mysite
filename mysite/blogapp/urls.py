from .views import ArticlesListView
from django.urls import path

app_name = 'blogapp'

urlpatterns = [
    path('', ArticlesListView.as_view(), name='article_list'),
]
