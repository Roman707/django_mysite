# from django.shortcuts import render
from django.views.generic import ListView
from .models import Article


class ArticlesListView(ListView):
    template_name = 'blog/article_list.html'
    context_object_name = 'articles'
    queryset = (
        Article.objects
        .defer('content')
        .select_related('author')
        .select_related('category')
        .prefetch_related('tags')
    )
