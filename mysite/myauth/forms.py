from django.forms import ModelForm
from myauth.models import Profile, User
from django.contrib.auth.models import User


class UserForm(ModelForm):
    class Meta:
        model = Profile
        fields = 'profile_picture', 'bio',
