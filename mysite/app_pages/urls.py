from django.urls import path
from .views import translation_example, greetings_page

app_name = 'app_pages'

urlpatterns = [
    path('example/', translation_example, name='example'),
    path('greetings/', greetings_page, name='greetings_page'),
]